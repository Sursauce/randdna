#include <iostream>
#include <random>

using namespace std;


string randDNA(int s, string b, int n) {
	mt19937 engine(s);
	int min = 1;
	int max = b.length();
	string gene;
	
	uniform_int_distribution<> unifrm(min, max);
	
	for (int i = 1; i <= n;i++){
		
		switch(unifrm(s)) {
			case 1: gene += b[0];
			break;
			case 2: gene += b[1];
			break;
			case 3: gene += b[2];
			break;
			case 4: gene += b[3];
			break;
			case 5: gene += b[4];
			break;
			case 6: gene += b[5];
			break;
			case 7: gene += b[6];
			break;
		}
	}

return gene;

}

